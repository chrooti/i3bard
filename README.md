# i3bard - an i3 bar daemon

i3bard is an async, event-driven i3bar `status_command` generator that can synchronize multiple instances (e.g. if you have multiple screens).

## Usage

```
usage: i3bard [-h] [-d] [-s PATH] [-c PATH] [-L PATH] [-q]

async i3bar status_command generator that can synchronize multiple instances

options:
  -h, --help            show this help message and exit
  -d, --daemon          run the daemon
  -s PATH, --socket PATH
                        socket (default: /run/user/1000/i3bard)
  -c PATH, --config PATH
                        config file (default: /home/strychnide/.config/i3bard/config.toml)
  -L PATH, --cmdlet-path PATH
                        additional cmdlet dir (default: /home/strychnide/.local/share/i3bard/cmdlets)
  -q, --quiet           Suppress _only_ stdout (overridable via SIGUSR1/2)
```

## Quick start

Set `status_command i3bard` in the bar section of your i3 config file.
If you're using systemd socket activation should take care of the rest, else you should:
- run a daemon instance of i3bard by passing the `-d` option
- decide a suitable socket location if the default (`$XDG_RUNTIME_DIR/i3bard`) doesn't work for you

## Configuration

### Mental model

i3bard allows you to define multiple `blocks`, each block is an instance of a commandlet (`cmdlet`): a task that runs concurrently* with the others and modifies the block content that gets sent to [i3bar](https://i3wm.org/docs/i3bar-protocol.html).
The simplest block is `exec`: it runs a shell command at a given interval or when a file changes and optionally prepends the result with a given label.

\* in the cooperative multitasking sense

### Format

i3bard uses the toml format, the default config path is either `$XDG_CONFIG_HOME/i3bard/config.toml` or `~/.config/i3bard/config.toml`, but it can be changed by passing the `-c $CONFIG_PATH` option.
A config example is available in the root of the repository, a full description follows.

There are three "root" sections:
- `defaults`: contains attributes that are common to each block in order to avoid redefining them each time
- `options`: miscellaneous client/server options
- `blocks`: the meat of the configuration, an array of tables with three options:
    - `cmdlet`: the name of the cmdlet, aka its relative path without the .py extension. Cmdlets are loaded from the following paths:
        - `/usr/lib/i3bard/cmdlets`
        - `$XDG_DATA_HOME/i3bard/cmdlets` or `$HOME/.local/share/i3bard/cmdlets`
        - any other path passed with the `-L` option
    If the value is an empty string no command it's run and the block is considered static.
    - `args`: a table of arguments, they depend on the cmdlet
        - the most common are `interval` (that decides how many seconds should pass between each cmdlet invocation) and `label` that allows you to prepend some static text to the command
        - the arguments accepted by each cmdlet can be found in the `Config` class of each cmdlet file
    - `properties`: a table of attributes that will be merged into the block at startup, requires at least the `name` property
The configuration schema can be found in the [types.py](src/i3bard/types.py) file.

## Cmdlets API

Each module should export 3 async functions:
- `setup`: called once with content of the `args` section
- `teardown`: called when the app is stopped
- `run`: called once and eventually again each time it raises an Exception with a non-configurable 1 second interval. If the function returns normally the block is considered static afterwards. It receives as argument both the block to modify and an `asyncio.Event` that must be set in order for the server to pickup the changes (but not setting it doesn't prevent it).
See the `cmdlets` directory under the source code for examples.

i3bard also duplicates as a library, each part (especically `i3bard.types` and `i3bard.utils`) can be imported to simplify the task of writing custom cmdlets.
