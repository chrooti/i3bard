from __future__ import annotations

import asyncio
import os
import time
import traceback
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import repeat_at_interval
from i3bard.utils.watch import unwatch, watch


class Config:
    def __init__(
        self,
        *,
        cmd: str,
        env: list[str] | None = None,
        label: str = "",
        interval: float | int = 1.0,
        files: list[str] | None = None,
    ) -> None:
        self.cmd = cmd

        self.env: dict[str, str] = {
            key: value or os.environ.get(key, "")
            for key, _equal, value in (envvar.partition("=") for envvar in env or {})
        }

        self.label = f"{label} " if label else ""
        self.interval = float(interval)
        self.files = files or []


config: Config


async def setup(**kwargs: t.Any) -> None:
    global config

    config = Config(**kwargs)


async def teardown() -> None:
    pass


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async def callback() -> None:
        try:
            process = await asyncio.create_subprocess_shell(
                config.cmd,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.STDOUT,
                env=config.env,
            )
            await process.wait()
        except Exception as e:
            output = repr(e)
        else:
            assert process.stdout is not None

            output = (await process.stdout.read()).removesuffix(b"\n").decode()

        block["full_text"] = f"{config.label}{output}"
        event.set()

    watched_descriptors = []
    try:
        for file in config.files:
            wd = await watch(file.encode(), callback)
            watched_descriptors.append(wd)
    except Exception as e:
        # clean already watched descriptors to avoid any inconsistency
        for wd in watched_descriptors:
            try:
                unwatch(wd, callback)
            except:
                pass

        message = traceback.format_exception_only(e)[0]
        block["full_text"] = f"{config.label}{message}"
        event.set()

        # raise: the daemon will retry this block after 1 second
        # and we don't remain stuck in an inconsistent state
        raise e

    # call callback once to populate the block
    if config.interval < 0:
        await callback()
        return

    async for _ in repeat_at_interval(config.interval):
        begin_time = time.time()

        await callback()

        sleep_time = config.interval - (time.time() - begin_time)
        await asyncio.sleep(sleep_time)
