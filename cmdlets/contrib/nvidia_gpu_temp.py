from __future__ import annotations

import asyncio
import ctypes
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import repeat_at_interval


class Config:
    def __init__(
        self,
        *,
        device_uuid: str,
        label: str = "",
        interval: float | int = 5,
    ) -> None:
        self.device_uuid = device_uuid
        self.label = f"{label} " if label else ""
        self.interval = float(interval)


class Nvml:
    def __init__(self, device_uuid: str) -> None:
        self.nvml = ctypes.cdll.LoadLibrary("libnvidia-ml.so")
        if self.nvml.nvmlInit_v2():
            raise Exception("nvmlInit")

        self.device_handle = ctypes.c_void_p()
        if self.nvml.nvmlDeviceGetHandleByUUID(
            device_uuid.encode(),
            ctypes.byref(self.device_handle),
        ):
            raise Exception("nvmlDeviceGetHandleByUUID")

        self.nvmlDeviceGetTemperature = self.nvml.nvmlDeviceGetTemperature

    def read_temperature(self) -> int:
        temperature = ctypes.c_int(0)
        if self.nvmlDeviceGetTemperature(
            self.device_handle, 0, ctypes.byref(temperature)
        ):
            raise Exception("nvmlDeviceGetTemperature")

        return temperature.value

    def __del__(self) -> None:
        self.nvml.nvmlShutdown()


config: Config
nvml: Nvml


async def setup(**kwargs: t.Any) -> None:
    global config, nvml

    config = Config(**kwargs)
    nvml = Nvml(config.device_uuid)


async def teardown() -> None:
    pass


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async for _ in repeat_at_interval(config.interval):
        block["full_text"] = f"{config.label}{nvml.read_temperature()} °C"
        event.set()
