from __future__ import annotations

import asyncio
import itertools
import os
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import poll_file

HWMON_DIR = "/sys/class/hwmon"


class Config:
    def __init__(
        self,
        *,
        name: str,
        temp_label: str,
        label: str | None = None,
        interval: float | int = 1.0,
    ) -> None:
        # sys/class/hwmon/hwmon*/name
        self.name = name

        # sys/class/hwmon/hwmon*/temp*_label
        self.temp_label = temp_label

        self.interval = float(interval)
        self.label = label or f"{name}.{temp_label}"


config: Config
tempfile: str


def readfile(path: str) -> str:
    with open(path, mode="r") as f:
        return f.read()


async def setup(**kwargs: t.Any) -> None:
    global config, tempfile

    config = Config(**kwargs)

    try:
        adapter_dir = next(
            f"{HWMON_DIR}/{adapter_dir}"
            for adapter_dir in os.listdir(HWMON_DIR)
            if config.name == readfile(f"{HWMON_DIR}/{adapter_dir}/name").strip()
        )
    except StopIteration:
        raise Exception(f'"{config.name}" hwmon adapter not found')

    try:
        tempfile = next(
            f"{adapter_dir}/temp{index}_input"
            for index in itertools.count(1)
            if config.temp_label == readfile(f"{adapter_dir}/temp{index}_label").strip()
        )
    except FileNotFoundError:
        raise Exception(
            f'"{config.temp_label}" label in "{config.name}" hwmon adapter not found'
        )


async def teardown() -> None:
    pass


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async for temp in poll_file(tempfile, config.interval):
        block["full_text"] = f"{config.label} {int(temp) // 1000} °C"
        event.set()
