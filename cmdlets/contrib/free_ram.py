from __future__ import annotations

import asyncio
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import poll_file

ONE_GIGABYTE_IN_KILOBYTES = 1 << 20


class Config:
    def __init__(
        self,
        *,
        interval: float | int = 0.5,
    ) -> None:
        self.format = format
        self.interval = float(interval)


config: Config


async def setup(**kwargs: t.Any) -> None:
    global config

    config = Config(**kwargs)


async def teardown() -> None:
    pass


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async for meminfo in poll_file("/proc/meminfo", config.interval, encoding="ascii"):
        total: int | None = None
        available: int | None = None
        for line in meminfo.splitlines():
            match line.split():
                case "MemTotal:", total_, "kB":
                    total = int(total_)

                case "MemAvailable:", available_, "kB":
                    available = int(available_)

                case _:
                    continue

        assert total is not None and available is not None

        used_gbs = (total - available) / ONE_GIGABYTE_IN_KILOBYTES
        used_gbs = round(used_gbs, 0 if used_gbs > 10 else 1)

        block["full_text"] = f"RAM {used_gbs}G"
        event.set()
