from __future__ import annotations

import asyncio
import collections.abc as c
import ctypes
import socket
import typing as t

from i3bard.types import BlockProperties

UINT_MAX = 4294967295


class Consts:
    ARPHRD_ETHER = 1
    IFA_ADDRESS = 1
    IFA_LOCAL = 2
    IFA_LABEL = 3
    IFLA_IFNAME = 3
    IFF_UP = 1
    NETLINK_CAP_ACK = 10
    NLM_F_REQUEST = 1
    NLM_F_MULTI = 2
    NLM_F_ACK = 4
    NLM_F_ROOT = 0x100
    NLMSG_ERROR = 2
    NLMSG_DONE = 3
    RTM_NEWLINK = 16
    RTM_GETLINK = 18
    RTM_NEWADDR = 20
    RTM_GETADDR = 22
    RTMGRP_LINK = 1
    RTMGRP_IPV4_IFADDR = 0x10
    RTMGRP_IPV6_IFADDR = 0x100
    SOL_NETLINK = 270


class Iovec(ctypes.Structure):
    _fields_ = (
        ("iov_base", ctypes.c_void_p),
        ("iov_len", ctypes.c_size_t),
    )

    @classmethod
    def new(cls, buffer: t.Any) -> t.Self:
        return cls(ctypes.addressof(buffer), ctypes.sizeof(buffer))


class Nlmsghdr(ctypes.Structure):
    len: int
    type: int
    flags: int
    seq: int
    pid: int

    _fields_ = (
        ("len", ctypes.c_uint32),
        ("type", ctypes.c_uint16),
        ("flags", ctypes.c_uint16),
        ("seq", ctypes.c_uint32),
        ("pid", ctypes.c_uint32),
    )

    @classmethod
    def new(
        cls,
        *,
        len: int = 0,
        type: int = 0,
        flags: int = 0,
        seq: int = 0,
        pid: int = 0,
    ) -> t.Self:
        return cls(len, type, flags, seq, pid)


class Nlmsgerr(ctypes.Structure):
    error: int
    header: Nlmsghdr

    _fields_ = (
        ("error", ctypes.c_int),
        ("header", Nlmsghdr),
    )


class Ifinfomsg(ctypes.Structure):
    family: socket.AddressFamily
    type: int
    index: int
    flags: int
    change: int

    _fields_ = (
        ("family", ctypes.c_ubyte),
        ("type", ctypes.c_ushort),
        ("index", ctypes.c_int),
        ("flags", ctypes.c_uint),
        ("change", ctypes.c_uint),
    )

    @classmethod
    def new(
        cls,
        *,
        family: socket.AddressFamily = socket.AF_UNSPEC,
        type: int = 0,
        index: int = 0,
        flags: int = 0,
        change: int = 0xFFFFFFFF,
    ) -> t.Self:
        return cls(family, type, index, flags, change)


class Ifaddrmsg(ctypes.Structure):
    family: socket.AddressFamily
    prefixlen: int
    flags: int
    scope: int
    index: int

    _fields_ = (
        ("family", ctypes.c_ubyte),
        ("prefixlen", ctypes.c_ubyte),
        ("flags", ctypes.c_ubyte),
        ("scope", ctypes.c_ubyte),
        ("index", ctypes.c_uint),
    )

    @classmethod
    def new(
        cls,
        *,
        family: socket.AddressFamily = socket.AF_INET,
        prefixlen: int = 0,
        flags: int = 0,
        scope: int = 0,
        index: int = 0,
    ) -> t.Self:
        return cls(family, prefixlen, flags, scope, index)


class Rtattr(ctypes.Structure):
    len: int
    type: int

    _fields_ = (
        ("len", ctypes.c_ushort),
        ("type", ctypes.c_ushort),
    )


def parse_rtattrs(
    payload: bytearray, offset: int, msg_offset: int
) -> c.Generator[tuple[int, int, int], None, None]:
    while offset < msg_offset:
        rtattr = Rtattr.from_buffer(payload, offset)
        offset += aligned_rtattr_len
        data_len = rtattr.len - aligned_rtattr_len

        yield rtattr.type, offset, offset + data_len

        if data_len > 0:
            offset += align_len(data_len)


class IfinfoAttributes:
    def __init__(self) -> None:
        self.label: str = ""

    @classmethod
    def from_payload(
        cls,
        *,
        msg: Ifinfomsg,
        payload: bytearray,
        offset: int,
        msg_offset: int,
    ) -> t.Self:
        attributes = cls()

        for attr, start, end in parse_rtattrs(payload, offset, msg_offset):
            match attr:
                case Consts.IFLA_IFNAME:
                    attributes.label = payload[start : end - 1].decode()
                case _:
                    pass

        return attributes


class IfaddrAttributes:
    def __init__(self) -> None:
        self.label: str = ""
        self.ip: bytes = b""
        self.ip_string: str = ""

    @classmethod
    def from_payload(
        cls,
        *,
        msg: Ifaddrmsg,
        payload: bytearray,
        offset: int,
        msg_offset: int,
    ) -> t.Self:
        attributes = cls()

        for attr, start, end in parse_rtattrs(payload, offset, msg_offset):
            match attr:
                case Consts.IFA_LOCAL:
                    data = payload[start:end]
                    attributes.ip = data
                    attributes.ip_string = socket.inet_ntop(msg.family, data)
                case Consts.IFA_LABEL:
                    attributes.label = payload[start : end - 1].decode()
                case _:
                    pass

        return attributes


def align_len(value: int) -> int:
    return (value + 3) & ~3


aligned_header_len = align_len(ctypes.sizeof(Nlmsghdr))
aligned_error_len = align_len(ctypes.sizeof(Nlmsgerr))
aligned_ifinfomsg_len = align_len(ctypes.sizeof(Ifinfomsg))
aligned_ifaddrmsg_len = align_len(ctypes.sizeof(Ifaddrmsg))
aligned_rtattr_len = align_len(ctypes.sizeof(Rtattr))


class NetlinkSocket:
    def __init__(self, *, pid: int = 0, events: int = 0) -> None:
        self.sock = socket.socket(
            socket.AF_NETLINK, socket.SOCK_RAW, socket.NETLINK_ROUTE
        )
        self.sock.setsockopt(Consts.SOL_NETLINK, Consts.NETLINK_CAP_ACK, 1)
        self.sock.setblocking(False)

        self.sock.bind((pid, events))

        self.on_error: c.Callable[[Nlmsgerr], c.Awaitable[None]] = self.__noop
        self.on_ifaddrmsg: c.Callable[
            [Ifaddrmsg, IfaddrAttributes], c.Awaitable[None]
        ] = self.__noop
        self.on_ifinfomsg: c.Callable[
            [Ifinfomsg, IfinfoAttributes], c.Awaitable[None]
        ] = self.__noop

    async def __noop(self, *_args: t.Any) -> None:
        pass

    async def send(self, body: bytes) -> None:
        loop = asyncio.get_event_loop()

        await loop.sock_sendall(self.sock, body)

    async def recv(self) -> None:
        loop = asyncio.get_event_loop()

        # https://man7.org/linux/man-pages/man7/netlink.7.html
        # see read example
        largest_page_size = 8192

        while True:
            payload_bytes = await loop.sock_recv(self.sock, largest_page_size)
            payload = bytearray(payload_bytes)
            offset = 0
            msg_offset = 0

            while msg_offset < len(payload):
                header = Nlmsghdr.from_buffer(payload, msg_offset)

                offset = msg_offset + aligned_header_len
                msg_offset += header.len

                match header.type:
                    case Consts.NLMSG_ERROR:
                        error = Nlmsgerr.from_buffer(payload, offset)
                        await self.on_error(error)
                    case Consts.NLMSG_DONE:
                        return
                    case Consts.RTM_NEWLINK | Consts.RTM_GETLINK:
                        await self.handle_ifinfomsg(payload, offset, msg_offset)
                    case Consts.RTM_NEWADDR | Consts.RTM_GETADDR:
                        await self.handle_ifaddrmsg(payload, offset, msg_offset)
                    case _:
                        pass

    async def handle_ifinfomsg(
        self, payload: bytearray, offset: int, msg_offset: int
    ) -> None:
        msg = Ifinfomsg.from_buffer(payload, offset)
        offset += aligned_ifinfomsg_len

        attributes = IfinfoAttributes.from_payload(
            msg=msg,
            payload=payload,
            offset=offset,
            msg_offset=msg_offset,
        )

        await self.on_ifinfomsg(msg, attributes)

    async def handle_ifaddrmsg(
        self, payload: bytearray, offset: int, msg_offset: int
    ) -> None:
        msg = Ifaddrmsg.from_buffer(payload, offset)
        offset += aligned_ifaddrmsg_len

        attributes = IfaddrAttributes.from_payload(
            msg=msg,
            payload=payload,
            offset=offset,
            msg_offset=msg_offset,
        )

        await self.on_ifaddrmsg(msg, attributes)


class MsgHelper:
    def __init__(self) -> None:
        self.seq = 0

    def make_msg(self, type: int, flags: int, *parts: ctypes._CData) -> bytes:
        part_cdata: t.Iterable[ctypes._CData | t.Type[ctypes._CData]] = (
            Nlmsghdr,
            *parts,
        )
        msg_len = sum(ctypes.sizeof(part) for part in part_cdata)

        header = Nlmsghdr.new(
            len=msg_len,
            type=type,
            flags=flags,
            seq=self.seq,
        )

        self.seq = (self.seq + 1) % UINT_MAX

        return b"".join(bytes(part) for part in (header, *parts))


class Config:
    def __init__(self, *, interface: str, label: str | None = None) -> None:
        self.interface = interface
        self.label = label or interface


class State:
    def __init__(self, interface: str) -> None:
        self.interface = interface
        self.event = asyncio.Event()
        self.text = ""

    async def on_ifinfomsg(self, msg: Ifinfomsg, attrs: IfinfoAttributes) -> None:
        if self.interface != attrs.label:
            return

        if (msg.flags & Consts.IFF_UP) == 0:
            self.text = "down"
            self.event.set()

    async def on_ifaddrmsg(self, _msg: Ifaddrmsg, attrs: IfaddrAttributes) -> None:
        if self.interface != attrs.label:
            return

        self.text = attrs.ip_string
        self.event.set()


config: Config
sock: NetlinkSocket
state: State


async def setup(**kwargs: t.Any) -> None:
    global config, sock, state

    config = Config(**kwargs)
    state = State(config.interface)

    sock = NetlinkSocket(
        events=Consts.RTMGRP_LINK
        | Consts.RTMGRP_IPV4_IFADDR
        | Consts.RTMGRP_IPV6_IFADDR
    )
    sock.on_ifinfomsg = state.on_ifinfomsg
    sock.on_ifaddrmsg = state.on_ifaddrmsg

    msg_helper = MsgHelper()
    msg = msg_helper.make_msg(
        Consts.RTM_GETLINK,
        Consts.NLM_F_REQUEST | Consts.NLM_F_ROOT,
        Ifinfomsg.new(),
    )
    await sock.send(bytes(msg))
    await sock.recv()

    msg = msg_helper.make_msg(
        Consts.RTM_GETADDR,
        Consts.NLM_F_REQUEST | Consts.NLM_F_ROOT,
        Ifaddrmsg.new(),
    )
    await sock.send(bytes(msg))
    await sock.recv()


async def teardown() -> None:
    sock.sock.close()


async def set_text(block: BlockProperties, event: asyncio.Event) -> None:
    while True:
        await state.event.wait()
        state.event.clear()

        block["full_text"] = f"{config.label} {state.text}"
        event.set()


async def recv_loop() -> None:
    while True:
        await sock.recv()


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    block["full_text"] = f"{config.label} ..."

    await asyncio.gather(
        set_text(block, event),
        recv_loop(),
    )
