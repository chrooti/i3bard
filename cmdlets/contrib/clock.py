from __future__ import annotations

import asyncio
import datetime
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import repeat_at_interval


class Config:
    def __init__(
        self,
        *,
        format: str = "%a %d %b %Y - %H:%M:%S",
        interval: float | int = 0.5,
    ) -> None:
        self.format = format
        self.interval = float(interval)


config: Config


async def setup(**kwargs: t.Any) -> None:
    global config

    config = Config(**kwargs)


async def teardown() -> None:
    pass


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async for _ in repeat_at_interval(config.interval):
        block["full_text"] = datetime.datetime.now().strftime(config.format)
        event.set()
