from __future__ import annotations

import asyncio
import os
import typing as t

from i3bard.types import BlockProperties
from i3bard.utils import repeat_at_interval

ONE_GIGABYTE = 1 << 30


class Config:
    def __init__(
        self,
        *,
        mountpoint: str,
        label: str | None = None,
        interval: float | int = 5.0,
    ) -> None:
        self.mountpoint = mountpoint
        self.label = label or mountpoint
        self.interval = float(interval)


config: Config
fd: int


async def setup(**kwargs: t.Any) -> None:
    global config, fd

    config = Config(**kwargs)
    fd = os.open(config.mountpoint, flags=0)


async def teardown() -> None:
    os.close(fd)


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    async for _ in repeat_at_interval(config.interval):
        stats = os.fstatvfs(fd)
        free_gbs = stats.f_bavail * stats.f_bsize // ONE_GIGABYTE

        block["full_text"] = f"{config.label} {free_gbs}G"
        event.set()
