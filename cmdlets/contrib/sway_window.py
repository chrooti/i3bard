from __future__ import annotations

import asyncio
import json
import os
import struct

from i3bard.types import BlockProperties

payload_header = struct.Struct("=6sII")


# reference: https://man.archlinux.org/man/sway-ipc.7.en
# magic - length - msg type - data
def pack_payload(type_: int, body: bytes = b"") -> bytes:
    return payload_header.pack(b"i3-ipc", len(body), type_) + body


def unpack_payload_header(payload: bytes, offset: int) -> tuple[int, int]:
    _i3ipc, length, type_ = payload_header.unpack_from(payload, offset)

    return type_, length


SUBSCRIBE = 2
WINDOW_EVENT = 0x80000003
request = pack_payload(SUBSCRIBE, json.dumps(["window"]).encode())
chunk_size = 4096

reader: asyncio.StreamReader
writer: asyncio.StreamWriter


async def setup() -> None:
    global reader, writer

    reader, writer = await asyncio.open_unix_connection(os.environ["SWAYSOCK"])
    writer.write(request)
    await writer.drain()


async def teardown() -> None:
    writer.close()

    await writer.wait_closed()


async def run(block: BlockProperties, event: asyncio.Event) -> None:
    while True:
        data = bytearray()
        offset = 0
        msg_offset = 0

        chunk = await reader.read(chunk_size)
        data.extend(chunk)
        while True:
            _type_, length = unpack_payload_header(data, msg_offset)
            offset = msg_offset + payload_header.size
            msg_offset += payload_header.size + length

            while len(data) < msg_offset:
                chunk = await reader.read(chunk_size)
                data.extend(chunk)

            body = data[offset:msg_offset].decode()
            decoded_body = json.loads(body)

            match decoded_body:
                case {
                    "change": "focus",
                    "container": {"focused": True, "name": name},
                } | {"change": "title", "container": {"focused": True, "name": name}}:
                    block["full_text"] = name
                    event.set()
                case _:
                    pass

            remaining_size = len(data) - msg_offset
            if remaining_size <= 0:
                assert remaining_size == 0
                break

            data = bytearray(data[msg_offset:])
            msg_offset = 0
            if remaining_size < payload_header.size:
                chunk = await reader.read(chunk_size)
                data.extend(chunk)
