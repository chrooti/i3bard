from __future__ import annotations

import json
import signal
import sys
import types

from .types import BlockProperties


class Writer:
    def __init__(self, is_writing: bool = True) -> None:
        self.is_writing = is_writing

        # NOTE: this assumes one writer per process
        signal.signal(signal.SIGUSR1, self.pause)
        signal.signal(signal.SIGUSR2, self.unpause)

    def pause(self, _sig: int, _frame: types.FrameType | None) -> None:
        self.is_writing = True

    def unpause(self, _sig: int, _frame: types.FrameType | None) -> None:
        self.is_writing = False

    def write_header(self) -> None:
        """Print the connection header, sent to each connection once at the beginning"""

        if not self.is_writing:
            return

        header = json.dumps(
            {
                "version": 1,
                # default i3 signals can't be intercepted
                "stop_signal": signal.SIGUSR1.value,
                "cont_signal": signal.SIGUSR2.value,
                # TODO: allow this to be enabled
                "click_events": False,
            }
        )

        sys.stdout.write(f"{header}\n[\n")
        sys.stdout.flush()

    def write(self, *chunks: bytes) -> None:
        if not self.is_writing:
            return

        sys.stdout.write(f"{b''.join(chunks).decode()}\n")
        sys.stdout.flush()

    def write_error(self, error: str) -> None:
        if not self.is_writing:
            return

        error_block: BlockProperties = {
            "full_text": error,
            "name": "__ERROR__",
            "color": "#ff0000",
        }

        sys.stdout.write(
            f"{json.dumps([error_block], ensure_ascii=False)},\n\n",
        )
        sys.stdout.flush()
