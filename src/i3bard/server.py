from __future__ import annotations

import asyncio
import fcntl
import json
import logging
import os
import signal
import socket
import types

from .cmdlet import Cmdlet
from .utils import wait_for_many
from .writer import Writer


class Server:
    def __init__(
        self,
        *,
        cmdlets: tuple[Cmdlet, ...],
        socket: str,
        verbose: bool = True,
        setup_timeout: float | int = 60,
        teardown_timeout: float | int = 60,
    ) -> None:
        self.cmdlets = cmdlets
        self.data = tuple(cmdlets.properties for cmdlets in self.cmdlets)

        self.socket_path = socket

        self.clients: list[asyncio.StreamWriter] = []
        self.writer = Writer(is_writing=verbose)

        self.setup_timeout = setup_timeout
        self.teardown_timeout = teardown_timeout

        self.block_changed_event = asyncio.Event()

    async def main(self) -> None:
        if os.environ.get("LISTEN_FDS"):
            socket_fd = 3  # do not ask
            fcntl.fcntl(socket_fd, fcntl.FD_CLOEXEC)
            self.socket = socket.fromfd(socket_fd, socket.AF_UNIX, socket.SOCK_STREAM)
        else:
            try:
                os.remove(self.socket_path)
            except OSError:
                pass

            self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.socket.bind(self.socket_path)

        try:
            await self.do_main()
        except Exception:
            logging.exception("Exception raised during server loop")

        self.socket.close()

    async def do_main(self) -> None:
        """
        Main coroutine:
        - instantiates block coroutines
        - instantiates the server
        - waits until it's time to close
        """
        await wait_for_many(
            *(asyncio.ensure_future(cmdlet.setup()) for cmdlet in self.cmdlets),
            timeout=self.setup_timeout,
        )

        cmdlet_tasks = tuple(
            asyncio.create_task(cmdlet.run(self.block_changed_event))
            for cmdlet in self.cmdlets
        )

        self.writer.write_header()
        asyncio.create_task(self.write_data())

        # start the server and keep a reference to the writers only
        server = await asyncio.start_unix_server(
            lambda _, writer: self.clients.append(writer),
            sock=self.socket,
        )

        self.sigint_dfl = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, self.terminate)

        self.server_task = asyncio.ensure_future(server.serve_forever())
        try:
            await self.server_task
        except asyncio.CancelledError:
            pass

        for cmdlet_task in cmdlet_tasks:
            cmdlet_task.cancel()

        await wait_for_many(
            *(asyncio.ensure_future(cmdlet.teardown()) for cmdlet in self.cmdlets),
            timeout=self.setup_timeout,
        )

    async def write_data(self) -> None:
        """
        Writer coroutine, dumps the output of every block each
        `interval` seconds
        """

        while True:
            await self.block_changed_event.wait()

            # add a newline to the json (!)
            data = f"{json.dumps(self.data, ensure_ascii=False)},\n".encode()

            for client in self.clients:
                client.write(data)

            self.writer.write(data)

            # wait in order not to error on the next write
            results = await asyncio.gather(
                *(client.drain() for client in self.clients), return_exceptions=True
            )

            # filter out clients that errored or are closing
            self.clients = [
                client
                for client, result in zip(self.clients, results)
                if not client.is_closing() and not isinstance(result, Exception)
            ]

            self.block_changed_event.clear()

    def terminate(self, signum: int, _frame: types.FrameType | None) -> None:
        signal.signal(signum, self.sigint_dfl)
        self.server_task.cancel()
