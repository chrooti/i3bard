from __future__ import annotations

import logging
import socket
import time

from .writer import Writer


class Client:
    BACKOFF_TIMEOUT = 10

    def __init__(
        self,
        socket: str,
        verbose: bool = True,
        timeout: float | int = 2.0,
        chunk_size: int = 4096,
    ) -> None:
        self.socket_path = socket
        self.timeout = float(timeout)
        self.chunk_size = chunk_size

        self.writer = Writer(is_writing=verbose)

    def connect_and_write(self) -> None:
        client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        client.settimeout(self.timeout)
        client.connect(self.socket_path)

        # reads data in chunks of `chunk_size` bytes
        # and writes them to stdout
        while True:
            data = []
            while True:
                chunk = client.recv(self.chunk_size)
                if len(chunk) == 0:
                    client.close()
                    return

                data.append(chunk)
                if len(chunk) != self.chunk_size:
                    break

            self.writer.write(*data)

    def main(self) -> None:
        """
        Client task, listens in a blocking way on the socket and writees everything on stdout
        """

        self.writer.write_header()

        while True:
            try:
                self.connect_and_write()
            except Exception as e:
                self.writer.write_error(repr(e))
                logging.exception("Client error")

            # Non-configurable on purpose: this is a safety net
            # to avoid spamming the cpu
            time.sleep(self.BACKOFF_TIMEOUT)
