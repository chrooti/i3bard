from __future__ import annotations

import argparse
import asyncio
import importlib.machinery
import importlib.util
import os
import pwd
import tomllib
import typing as t

from .client import Client
from .cmdlet import Cmdlet
from .server import Server
from .types import BlockProperties, Config


def read_config(path: str) -> Config:
    with open(path, mode="rb") as f:
        config = t.cast(Config, tomllib.load(f))

    if defaults := config.get("defaults"):
        base_defaults: BlockProperties = {"name": "__NONAME__"}
        defaults = base_defaults | defaults
        for block in config["blocks"]:
            block["properties"] = defaults | block["properties"]

    return config


def load_cmdlet_specs(path: str) -> dict[str, importlib.machinery.ModuleSpec]:
    return {
        os.path.relpath(name, path): spec
        for file, name, ext in (
            (file, *os.path.splitext(file))
            for root, _dirs, files in os.walk(path)
            for file in (f"{root}/{file}" for file in files)
        )
        if ext == ".py" and (spec := importlib.util.spec_from_file_location(name, file))
    }


def main() -> None:
    lib_dir = "/usr/lib"
    home_dir = os.environ.get("HOME", pwd.getpwuid(os.getuid()).pw_dir)
    xdg_config_home = os.environ.get("XDG_CONFIG_HOME", f"{home_dir}/.config")
    xdg_data_home = os.environ.get("XDG_DATA_HOME", f"{home_dir}/.local/share")
    xdg_runtime_dir = os.environ.get("XDG_RUNTIME_DIR")
    socket_path = f"{xdg_runtime_dir}/i3bard" if xdg_runtime_dir else None

    parser = argparse.ArgumentParser(
        prog="i3bard",
        description="async i3bar status_command generator that can synchronize multiple instances",
    )
    parser.add_argument(
        "-d",
        "--daemon",
        help="run the daemon",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--socket",
        help="socket (default: %(default)s)",
        default=socket_path,
        metavar="PATH",
    )
    parser.add_argument(
        "-c",
        "--config",
        help="config file (default: %(default)s)",
        default=f"{xdg_config_home}/i3bard/config.toml",
        metavar="PATH",
    )
    parser.add_argument(
        "-L",
        "--cmdlet-path",
        help="additional cmdlet directory, can be passed multiple times",
        nargs="*",
        metavar="PATH",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        help="Suppress _only_ stdout (overridable via SIGUSR1/2)",
        action="store_true",
    )
    args = parser.parse_args()

    config = read_config(args.config)
    cmdlet_paths = (
        f"{lib_dir}/i3bard/cmdlets",
        f"{xdg_data_home}/i3bard/cmdlets",
        *(args.cmdlet_path or ()),
    )
    cmdlet_specs = {
        name: spec
        for path in cmdlet_paths
        for name, spec in load_cmdlet_specs(path).items()
    }

    if args.daemon:
        server_options = config.get("options", {}).get("server", {})

        cmdlets = tuple(Cmdlet(**block) for block in config["blocks"])
        for cmdlet in cmdlets:
            cmdlet.try_load_cmdlet(cmdlet_specs)

        server = Server(
            cmdlets=cmdlets,
            socket=args.socket,
            verbose=not args.quiet,
            **server_options,
        )
        asyncio.run(server.main())
    else:
        client_options = config.get("options", {}).get("client", {})
        client = Client(
            socket=args.socket,
            verbose=not args.quiet,
            **client_options,
        )
        client.main()
