from __future__ import annotations

import asyncio
import asyncio.events
import collections.abc as c
import ctypes
import ctypes.util
import os
import typing as t


class Consts:
    IN_NONBLOCK = 0o4000
    IN_CLOSE_WRITE = 0x8
    IN_MOVE_SELF = 0x800
    IN_IGNORED = 0x8000


class InotifyEventHeader(ctypes.Structure):
    wd: int
    mask: int
    cookie: int
    len: int

    _fields_ = (
        ("wd", ctypes.c_int32),
        ("mask", ctypes.c_uint32),
        ("cookie", ctypes.c_uint32),
        ("len", ctypes.c_uint32),
    )


INOTIFY_EVENT_HEADER_SIZE = ctypes.sizeof(InotifyEventHeader)
INOTIFY_READ_SIZE = 8192

WatchCallback = c.Callable[[], c.Coroutine[None, t.Any, None]]


class WatchException(Exception):
    pass


class Watcher:
    def __init__(self) -> None:
        library_path = ctypes.util.find_library("c") or "libc.so.6"
        library = ctypes.cdll.LoadLibrary(library_path)

        self.inotify_init = library.inotify_init1
        self.inotify_init.argtypes = (ctypes.c_int,)
        self.inotify_init.restype = ctypes.c_int

        self.inotify_add_watch = library.inotify_add_watch
        self.inotify_add_watch.argtypes = (
            ctypes.c_int,
            ctypes.c_char_p,
            ctypes.c_uint32,
        )
        self.inotify_add_watch.restype = ctypes.c_int

        self.inotify_rm_watch = library.inotify_rm_watch
        self.inotify_rm_watch.argtypes = (ctypes.c_int, ctypes.c_int)
        self.inotify_rm_watch.restype = ctypes.c_int

        self.inotify_fd = -1
        self.callbacks_by_fd: dict[int, set[WatchCallback]] = {}

    async def ensure_started(self) -> None:
        # async because we use loop.add_reader

        if self.inotify_fd != -1:
            return

        self.inotify_fd = self.inotify_init(Consts.IN_NONBLOCK)
        if self.inotify_fd == -1:
            raise WatchException("Couldn't start file watcher")

        loop = asyncio.get_running_loop()
        loop.add_reader(self.inotify_fd, self.callback)

    def callback(self) -> None:
        events = b""
        while True:
            chunk = os.read(self.inotify_fd, INOTIFY_READ_SIZE)
            events += chunk
            if len(chunk) != INOTIFY_READ_SIZE:
                break

        offset = 0
        while offset < len(events):
            header = InotifyEventHeader.from_buffer_copy(events, offset)
            offset += INOTIFY_EVENT_HEADER_SIZE + header.len

            if header.mask & Consts.IN_IGNORED:
                del self.callbacks_by_fd[header.wd]
                continue

            if header.mask & Consts.IN_MOVE_SELF:
                del self.callbacks_by_fd[header.wd]
                continue

            for callback in self.callbacks_by_fd[header.wd]:
                asyncio.create_task(callback())

    async def watch(self, path: bytes, callback: WatchCallback) -> int:
        await self.ensure_started()

        wd: int = self.inotify_add_watch(
            self.inotify_fd, path, Consts.IN_CLOSE_WRITE | Consts.IN_MOVE_SELF
        )
        if wd == -1:
            raise WatchException(f"Couldn't watch file {path.decode()}")

        callbacks = self.callbacks_by_fd.setdefault(wd, set())
        callbacks.add(callback)

        return wd

    def unwatch(self, wd: int, callback: WatchCallback) -> None:
        result = self.inotify_rm_watch(self.inotify_fd, wd)
        if result == -1:
            raise WatchException(f"Couldn't unwatch fd {wd}")

        self.callbacks_by_fd[wd].remove(callback)


watcher = Watcher()
watch = watcher.watch
unwatch = watcher.unwatch
