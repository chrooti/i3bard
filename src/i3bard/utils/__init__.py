from __future__ import annotations

import asyncio
import collections.abc as c
import time
import typing as t


async def repeat_at_interval(interval: float) -> c.AsyncGenerator[None, None]:
    while True:
        begin_time = time.time()

        yield

        sleep_time = interval - time.time() + begin_time
        await asyncio.sleep(sleep_time)


async def poll_file(
    path: str, interval: float, **open_args: t.Any
) -> c.AsyncGenerator[str, None]:
    async for _ in repeat_at_interval(interval):
        with open(path, mode="r", **open_args) as f:
            yield f.read()


async def wait_for_many(  # type: ignore[no-untyped-def]
    *coros_or_futures, timeout: int | float | None = None
):
    return await asyncio.wait_for(asyncio.gather(*coros_or_futures), timeout=timeout)
