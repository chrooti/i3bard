from __future__ import annotations

import asyncio
import collections.abc as c
import importlib.machinery
import importlib.util
import logging
import types
import typing as t

from .types import BlockProperties


class InvalidCmdletName(Exception):
    pass


class Cmdlet:
    def __init__(
        self,
        *,
        cmdlet: str,
        args: dict[str, t.Any] | None = None,
        properties: BlockProperties,
        interval: float | int = 1.0,
    ) -> None:
        self.name = cmdlet
        self.args = args or {}
        self.properties = properties
        self.interval = float(interval)

        self.module: types.ModuleType

        self.setup: c.Callable[[], c.Awaitable[None]] = self.noop
        self.teardown: c.Callable[[], c.Awaitable[None]] = self.noop
        self.run: c.Callable[[asyncio.Event], c.Coroutine[None, None, None]] = self.noop

    def try_load_cmdlet(self, specs: dict[str, importlib.machinery.ModuleSpec]) -> None:
        if not self.name:
            return

        spec = specs.get(self.name)
        if not spec:
            raise InvalidCmdletName(self.name)

        assert spec.loader

        self.module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(self.module)

        self.setup = self.do_setup
        self.teardown = self.do_teardown
        self.run = self.do_run

    async def noop(self, *_args: t.Any) -> None:
        pass

    async def do_setup(self) -> None:
        try:
            await self.module.setup(**self.args)
        except Exception as e:
            # it's fine if we exit on setup since whatever service manager
            # will restart i3bard, however waiting for one second has better
            # chances of succeeding in case of race conditions
            await asyncio.sleep(1)
            raise e from None

    async def do_teardown(self) -> None:
        await self.module.teardown()

    async def do_run(self, event: asyncio.Event) -> None:
        while True:
            try:
                await self.module.run(self.properties, event)
            except Exception:
                logging.exception(f"Failure to run block \"{self.properties['name']}\"")
                await asyncio.sleep(1)
                continue
            else:
                break
