from __future__ import annotations

import typing as t


class Config(t.TypedDict, total=False):
    options: Options
    defaults: BlockProperties
    blocks: t.Required[list[Block]]


class Options(t.TypedDict, total=False):
    client: ClientOptions
    server: ServerOptions


class Block(t.TypedDict, total=False):
    cmdlet: str
    """Used to distinguish between static and dynamic block"""

    args: dict[str, t.Any]
    properties: t.Required[BlockProperties]


class ClientOptions(t.TypedDict, total=False):
    timeout: int | float
    chunk_size: int


class ServerOptions(t.TypedDict, total=False):
    setup_timeout: int | float
    teardown_timeout: int | float


class BlockProperties(t.TypedDict, total=False):
    """
    Properties derived from the i3 protocol, these are converted to json as is.

    The protocol: https://i3wm.org/docs/i3bar-protocol.html
    """

    full_text: str
    short_text: str
    color: str
    background: str
    border: str
    border_top: int
    border_right: int
    border_bottom: int
    border_left: int
    min_width: int
    align: str
    name: t.Required[str]
    instance: str
    urgent: bool
    separator: bool
    separator_block_width: int
    markup: str
